This is the backend of the landingpage form. 

This is the form of the landing page. 
Once the user enters the details the data gets stored into the database. And the user is taken to thank you page.
We can view the entries in the route 'list'. 

Necessary Packages to be installed 


Python 3
flask
flask_sqlalchemy
flask_migrate
flask_wtf
bootstrap (you can use CDN links) 

Step :1 Install all the above mentioned packages globally or in a virtual environment corresponding to the folder 



Step : 2 If you are in Mac use the following code to set the FLASK_APP 

export FLASK_APP=app.py 

Step :3 Once all the packages are installed 
Use the following commands to initiate the db 

flask db init 
flask db migrate 
flask db upgrate 

Sqlite data base will be created in the same folder with alembic versions available for migration. 

Step:4 Execute the app. 

python app.py 

You will see 

 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 
 you can go to that URL http://127.0.0.1:5000/ to see the landing page form. Once you enter the data it will take you to thank you page form. 
 To see the list of entries go to the link http://127.0.0.1:5000/list 
 
 
 
 
